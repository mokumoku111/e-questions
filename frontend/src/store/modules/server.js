import { getServer } from '../../constant/endpoint'
import axios from 'axios'
import _ from 'lodash'

const state = {
  loading: false,
  error: {},
  data: []
}
  
const getters = {
  // foods: state => state.foods
}
  
const actions = {
  // action
  getServer (context) {
    const url = getServer
    context.commit('LOADING', { loading: true })
    axios
      .get(url)
      .then((res) => {
        const server = _.get(res, 'data', [])
        // console.log(server)
        if (server.length > 0) {
          context.commit('SUCCESS', { server })
        }
      })
      .catch((e) => {
        context.commit('ERROR', { error: e })
      })
      .finally(() => {
        context.commit('LOADING', { loading: false })
      })
  }
}
  
const mutations = {
  // mutations
  UPDATE (state, payload) {
    state.data = {
      ...payload,
      ...state.data
    }
  },
  SUCCESS (state, payload) {
    state.data = payload.server
  },
  ERROR (state, payload) {
    state.error = {
      ...payload
    }
  },
  LOADING (state, payload) {
    state.loading = payload.loading
  }
}
  
export default {
  state,
  getters,
  actions,
  mutations
}
  