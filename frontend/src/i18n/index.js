//i18n.js
import Vue from 'vue'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
const messages = {
  'en': {
    'hi': 'Hi',
    'event': 'Event',
    'history': 'History'
  },
  'th': {
    'hi': 'สวัสดี',
    'event': 'โดรนขึ้นบิน',
    'history': 'ประวัติการบิน'
  }
}
const i18n = new VueI18n({
  locale: 'en', // set locale
  fallbackLocale: 'th', // set fallback locale
  messages //set locale messages
})
export default i18n