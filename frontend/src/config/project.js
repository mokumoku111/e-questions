const API_BASE_URL = process.env.VUE_APP_API_BASE_URL
const API = process.env.VUE_APP_API

export default {
  API_BASE_URL,
  API
}
