import project from '../config/project'

const { API_BASE_URL } = project

export const getServer = `${API_BASE_URL}/server-performance`

